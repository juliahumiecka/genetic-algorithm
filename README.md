# GA optimisation alghoritm GUI

Implementation of genetic alghoritm to minimalize Bochaczewski function 

## Installation


**Python 3.8** recommended.
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install -r requirements.txt
```

## Usage

```bash
python gui.py
```


## License
[MIT](https://choosealicense.com/licenses/mit/)