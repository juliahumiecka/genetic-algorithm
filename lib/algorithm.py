import copy
import random
import pandas as pd
import numpy as np
import math
import datetime

def Alpine_function(*x):
    x = np.array(list(x))
    return np.sum(np.fabs((x * np.sin(x) + 0.1 * x)))

def Bochaczewski_function(*x):
    x = np.array(list(x))[0]
    return x[0]**2 + 2 * x[1]**2 - 0.3 * math.cos(3 * math.pi * x[0] + 4 * math.pi * x[1]) + 0.3


class Genetic:
    def __init__(self,
                 selection_method='ranking',
                 selection_tournament_size=3,

                 crossover_method='onepoint',
                 crossover_probability=0.5,

                 mutation_method='twopoint',
                 mutation_probability=0.01,

                 inversion_probability=0.01,
                 elitist_strategy_percent=0.1,

                 population_size=50,
                 max_epochs=100,

                 target_function=Bochaczewski_function,
                 n_variables=2,
                 minim=-100,
                 maxim=100,

                 delta_x=0.001):

        
        self.supported_selection_methods = {
            "tournament": self.__tournament_selection,
            "ranking": self.__ranking_selection,
            "roulette": self.__roulette_selection,
        }

        self.supported_crossover_methods = {
            "onepoint": self.__onepoint_crossover,
            "twopoint": self.__twopoint_crossover,
            "threepoint": self.__threepoint_crossover,
            "uniform": self.__uniform_crossover
        }

        self.supported_mutation_methods = {
            "border": self.__border_mutation,
            "onepoint": self.__onepoint_mutation,
            "twopoint": self.__twopoint_mutation,
        }


        if selection_method not in self.supported_selection_methods:
            raise ValueError("invalid selection_method")

        if crossover_method not in self.supported_crossover_methods:
            raise ValueError("invalid cross_method")

        if mutation_method not in self.supported_mutation_methods:
            raise ValueError("invalid mutation_method")

        self.selection_method = self.supported_selection_methods[selection_method]
        self.crossover_method = self.supported_crossover_methods[crossover_method]
        self.mutation_method = self.supported_mutation_methods[mutation_method]

        # selection config
        self.selection_tournament_size = selection_tournament_size

        # genetic operators propability
        self.mutation_probability = mutation_probability
        self.crossover_probability = crossover_probability
        self.inversion_probability = inversion_probability

        # elitist strategy 
        self.elitist_size = int(population_size * elitist_strategy_percent)

        # function config
        self.fn = target_function
        self.n_variables = n_variables
        self.minim = minim
        self.maxim = maxim

        # generic config
        self.population_size = population_size
        self.epochs = max_epochs

        # encoding population config
        n_bits, epsilon = self.__get_encode_config(delta_x)
        self.n_bits = n_bits
        self.chromosom_size = n_bits * n_variables
        self.epsilon = epsilon

        # values used for charts and summary
        self.summary = {
            "epoch": [],
            "best_specimen": [],
            "standard_deviation": [],
            "mean_average": [],
            "elapsed_time": []
        }

        self.init_time = datetime.datetime.now()

    def __get_encode_config(self, delta_x):
        if self.minim > self.maxim:
            raise Exception("Invalid Range!")

        dx = math.fabs((self.minim - self.maxim)) / delta_x
        n_bits = math.floor(math.log2(dx) + 1)
        return n_bits, math.fabs(self.minim - self.maxim) / (2 ** n_bits - 1)

    def __initialize_population(self):
        return np.random.binomial(1, 0.5, (self.population_size, self.n_variables * self.n_bits))

    def __evaluate_population(self, population):

        decoded_population = self.__decode_population(population)
        evaluated_population = [self.fn(x) for x in decoded_population]

        ranking = list(zip(evaluated_population, population))
        ranking = np.array(ranking, dtype=[('Value', 'float'), ('Repr', 'object')])

        return np.copy(ranking[np.argsort(ranking['Value'])])

    def __decode_chromosom(self, chromosom):
        tab = []
        for t in np.split(chromosom, self.n_variables):
            d = np.sum(np.multiply(t, 2) ** [x for x in range(self.n_bits)])
            tab.append(self.minim + d * self.epsilon)
        return tab

    def __decode_population(self, population):
        return [self.__decode_chromosom(chromosom) for chromosom in population]
    
    def __selection_operator(self, population):
        elites = copy.deepcopy(population[:self.elitist_size])
        selected_population = copy.deepcopy(self.selection_method(population))

        return elites['Repr'], selected_population['Repr']

    def __roulette_selection(self, population):
        selection_size = self.population_size - self.elitist_size
    
        fitness_sum = np.sum(1 / population['Value'])
        probabilities = [(1 / specimen_value) / fitness_sum for specimen_value in population['Value']]
        selected = [np.random.choice(self.population_size, p=probabilities) for _ in range(selection_size)]
        return population[selected]

    def __ranking_selection(self, population):
        selection_size = self.population_size - self.elitist_size
        
        rank_size = self.population_size        
        rank_sum = sum(rank + 1 for rank in range(self.population_size))
        probabilities = [(rank_size - rank)/ rank_sum for rank in range(self.population_size)]
        selected = [np.random.choice(self.population_size, p=probabilities) for _ in range(selection_size)]

        return population[selected]

    def __tournament_selection(self, population):
        selection_size = self.population_size - self.elitist_size

        selected = []
        for _ in range(selection_size):
            tournament = np.random.choice(self.population_size, self.selection_tournament_size)
            # population is ordered by best so lowest index is champion
            selected.append(np.min(tournament))

        return population[selected]

    def __crossover_operator(self, population):
        np.random.shuffle(population)

        for chrom in range(population.size // 2):
            if self.crossover_probability >= random.random():
                p1, p2 = population[chrom], population[-(chrom + 1)]
                population[chrom], population[-(chrom + 1)] = self.crossover_method(p1, p2)

        return population

    def __npoint_crossover(self, p1, p2, n):
        cross_points = np.random.choice(self.chromosom_size, n, replace=False)
        cross_points = np.sort(cross_points)

        for k in cross_points:
            p1, p2 = np.append(p1[:k], p2[k:]), np.append(p2[:k], p1[k:])

        return p1, p2

    def __onepoint_crossover(self, p1, p2):
        return self.__npoint_crossover(p1, p2, 1)

    def __twopoint_crossover(self, p1, p2):
        return self.__npoint_crossover(p1, p2, 2)

    def __threepoint_crossover(self, p1, p2):
        return self.__npoint_crossover(p1, p2, 3)

    def __uniform_crossover(self, p1, p2):
        for cross_point in np.random.choice(self.chromosom_size, self.chromosom_size // 2, replace=False):
            p1[cross_point], p2[cross_point] = p2[cross_point], p1[cross_point]

        return p1, p2


    def __mutation_operator(self, population):
        for i in range(population.size):
            if self.mutation_probability >= random.random():
                population[i] = self.mutation_method(population[i])

            if self.inversion_probability >= random.random():
                population[i] = self.__inversion_mutation(population[i])

        return population

    def __onepoint_mutation(self, chromosom):
        mutation_points = np.random.choice(self.chromosom_size, 1, replace=False)
        chromosom[mutation_points] = 1 if chromosom[mutation_points] == 0 else 0
        return chromosom

    def __twopoint_mutation(self, chromosom):
        mutation_points = np.random.choice(self.chromosom_size, 2, replace=False)

        for m in mutation_points:
            chromosom[m] = 1 if chromosom[m] == 0 else 0

        return chromosom

    def __border_mutation(self, chromosom):
        # border = np.random.choice([1, -1])
        border = -1
        chromosom[border] = 1 if chromosom[border] == 0 else 0
        return chromosom

    def __inversion_mutation(self, chromosom):
        invert_points = np.random.choice(self.chromosom_size, 2, replace=False)
        lb, ub = np.sort(invert_points)
        chromosom[lb:ub] = np.flipud(chromosom[lb:ub])
        return chromosom

    def __collect_stats(self, epoch, start_time, end_time, pop_val):
        if not end_time:
            start_time = datetime.datetime.now()
            end_time = start_time

        self.summary["epoch"].append(epoch)
        self.summary["best_specimen"].append(pop_val[0])
        self.summary["standard_deviation"].append(np.std(pop_val))
        self.summary["mean_average"].append(np.mean(pop_val))
        self.summary["elapsed_time"].append(start_time - end_time)


    def learn(self):
        population = self.__initialize_population()
        end_time = None
        evaluated_population = None

        for epoch in range(self.epochs):
            start_time = datetime.datetime.now()

            evaluated_population = self.__evaluate_population(population)
            self.__collect_stats(epoch, start_time, end_time, evaluated_population['Value'])

            elites, selected_population = self.__selection_operator(evaluated_population)
            selected_population = self.__crossover_operator(selected_population)
            selected_population = self.__mutation_operator(selected_population)

            population = np.copy(np.stack(np.concatenate((elites, selected_population))))

            end_time = datetime.datetime.now()

        return pd.DataFrame.from_dict(self.summary), {
            "total_run_time": datetime.datetime.now() - self.init_time,
            "best_value_found":  evaluated_population['Value'][0]
        }

    def get_stats(self):
        return pd.DataFrame.from_dict(self.summary)
