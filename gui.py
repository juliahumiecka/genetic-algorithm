import PySimpleGUI as sg
import matplotlib
import matplotlib.pyplot as plt
import sqlite3 as sql
import numpy as np

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from random import random
from lib.algorithm import Genetic
from pandas.core.frame import DataFrame
from typing import Union, List

matplotlib.use('TkAgg')
sg.change_look_and_feel('DarkGrey10')


class Gui:
    OPTIMISATION_FUN_NAME = 'Bochaczewski'

    LAYOUT = [
        # GENERIC SECTION
        [
            sg.Frame(
                title='GA configuration:',
                pad=(20,10),
                layout=[[  
                    sg.Text('Binary quantization accuracy: '), sg.Input('0.001', key='delta_x', size=(5, 1)),
                    sg.Text('Population size: '), sg.Input('50', key='population_size', size=(5, 1)),
                    sg.Text('Epoch count: '), sg.Input('200', key='max_epochs', size=(5, 1))
                ]]
            )
        ],


        # SELECTION SECTION
        [
            sg.Frame(
                title='Selection configuration:',
                pad=(20,10),
                layout=[[   
                    sg.Text('Choose method of selection: '),
                    sg.Radio('Ranking', "RADIO1", key='selection_method-ranking', default=True),
                    sg.Radio('Roulette', "RADIO1", key='selection_method-roulette'),
                    sg.Radio('Tournament', "RADIO1", key='selection_method-tournament')
                ], [
                    sg.Text('Tournament size: '), sg.Input('5', key='selection_tournament_size', size=(5, 1))
                ]]
            )
        ],

        # CROSSOVER SECTION
        [
            sg.Frame(
                title='Crossover configuration:',
                pad=(20,10),
                layout=[
                    [sg.Text('Choose method of crossover: '),
                     sg.Radio('One-point', "RADIO2", key='crossover_method-onepoint', default=True),
                     sg.Radio('Two-point', "RADIO2", key='crossover_method-twopoint'),
                     sg.Radio('Three-point', "RADIO2", key='crossover_method-threepoint'),
                     sg.Radio('Uniform', "RADIO2", key='crossover_method-uniform')],
                    [sg.Text('Crossover probability: '), sg.Slider(range=(0, 100), orientation='h', key='crossover_probability', size=(34, 20), default_value=70)]
                ]
            )
        ],

        # MUTATION SECTION
        [
            sg.Frame(
                title='Mutation configuration:',
                pad=(20,10),
                layout=[
                    [sg.Text('Choose method of mutation: '),
                     sg.Radio('One-point', "RADIO3", key='mutation_method-onepoint', default=True),
                     sg.Radio('Two-point', "RADIO3", key='mutation_method-twopoint'),
                     sg.Radio('Border', "RADIO3", key='mutation_method-border')],
                    [sg.Text('Mutation probability: '), sg.Slider(range=(0, 100), key='mutation_probability', orientation='h', size=(34, 20), default_value=2)]
                ]
            )
        ],

        # EXTRA OPTIONS SECTION
        [
            sg.Frame(
                title='Choose extra options:',
                pad=(20,10),
                layout=[
                    [sg.Text('Inversion probability'), sg.Slider(range=(0, 100), orientation='h', size=(34, 20), key='inversion_probability', default_value=2)],
                    [sg.Text('Elitism percentage: '),  sg.Slider(range=(0, 100), orientation='h', size=(34, 20), key='elitist_strategy_percent', default_value=1)]
                ]
            )
        ],


                # EXTRA OPTIONS SECTION
        [
            sg.Frame(
                title='Results:',
                pad=(20,10),
                layout=[
                    [sg.Text('Best value'), sg.Text('Empty           ', key='-VALUE-')],
                    [sg.Text('Total run time: '), sg.Text('Empty             ', key='-TIME-', border_width=1)]
                ]
            )
        ],

        # RUN SECTION
        [sg.Button('Generate'), sg.Button('Exit')]
    ]

    def _generate_charts(self):
        pass

    def _draw_plot(self, data: DataFrame):
        fig, (ax1, ax2) = plt.subplots(2, sharex=True)
        fig.canvas.set_window_title('Bochaczewski function minimisation for x ⊂ [-100, 100]')

        ax1.plot(data['best_specimen'], label='function value')
        ax1.set_title('Value of best speciman in epoch')
        ax1.legend()

        ax2.plot(data['mean_average'], label='Mean average')
        ax2.plot(data['standard_deviation'], label='Standard deviation')
        ax2.set_title('Mean average and Standard deviation for all specimens in epoch')
        ax2.legend()

        ax1.set(ylabel='Value')
        ax2.set(xlabel='Epoch number', ylabel='Value')

        fig.tight_layout(pad=2.0)
        plt.show()

    @staticmethod
    def _get_method(input: dict, method_name: str):
        for key, value in input.items():
            if key.startswith(method_name) and value:
                return key.split('-')[1]

    @staticmethod
    def _cast_value(value: str, target: Union[int, float]) -> Union[int, float]:
        return target(value)
        
    def _parse_input(self, input):
        return {
            'selection_method': self._get_method(input, 'selection_method'),
            'selection_tournament_size': self._cast_value(input['selection_tournament_size'], int),
            'crossover_method': self._get_method(input, 'crossover_method'),
            'crossover_probability': input['crossover_probability'] / 100,
            'mutation_method': self._get_method(input, 'mutation_method'),
            'mutation_probability': input['mutation_probability'] / 100,
            'inversion_probability': input['inversion_probability'] / 100,
            'elitist_strategy_percent': input['elitist_strategy_percent'] / 100,
            'population_size': self._cast_value(input['population_size'], int),
            'max_epochs': self._cast_value(input['max_epochs'], int),
            'delta_x': self._cast_value(input['delta_x'], float),
        }

    def _save_to_db(self, data: DataFrame):
        db_name = f'results.db'
        db_connection = sql.connect(db_name)
        data[['best_specimen', 'standard_deviation', 'mean_average']].to_sql(
            'genetic_alghoritm_result', 
            db_connection,if_exists='replace')

    @staticmethod
    def _log_error(message, error):
        sg.popup_quick_message(f'exception: {error}', message, keep_on_top=True, background_color='red', text_color='white')

    def run(self):
        window = sg.Window('Matplotlib', self.LAYOUT, location=(0,0), keep_on_top=True, finalize=True)

        while True:
            event, values = window.read()

            if event == sg.WINDOW_CLOSED or event is None or event == 'Exit':
                break

            if event == 'Generate':
                try:
                    alg_params = self._parse_input(values)

                    df, stats = Genetic(**alg_params).learn()

                    window['-VALUE-'].update(round(stats["best_value_found"], 3))
                    window['-TIME-'].update(stats["total_run_time"])

                    self._save_to_db(df)
                    self._draw_plot(df)


                except ValueError as error:
                    print(error)
                    self._log_error('Invalid Data Provided', error)
                except Exception as error:
                    print(error)
                    self._log_error('Error ocurred', error)

        window.close()

Gui().run()
